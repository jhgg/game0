﻿//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(['Animation/List', 'Pools/BulletPool', 'Pools/TargetList', 'Pools/ParticlePool', 'Sprites/PlayerShip'],
function (AnimationList, BulletPool, TargetList, ParticlePool, PlayerShip) {
    "use strict";
    return AnimationList.extend({
        initPools: function() {
            this.bulletPool = new BulletPool(40);
            this.add(this.bulletPool);

            this.targetList = new TargetList();
            this.add(this.targetList);

            this.add(new PlayerShip());

            this.particlePool = new ParticlePool(2000);
            this.add(this.particlePool);
        },

        specialUpdate: function () {
            var bulletPool_data = this.bulletPool.data,
                bulletPool_length = bulletPool_data.length,
                targetPool_data = this.targetList.data,
                targetPool_length = targetPool_data.length,
                curBullet, curTarget;

            for (var i = 0; i < bulletPool_length; i++) {
                curBullet = bulletPool_data[i];

                if (!curBullet.isActive)
                    continue;

                for (var j = 0; j < targetPool_length; j++) {
                    curTarget = targetPool_data[j];
                    if (!curTarget.isActive)
                        continue;


                    if ((Math.abs(curTarget.X - curBullet.X) < (((curTarget.image.width * curTarget.scaleX) +
                        (curBullet.image.width * curBullet.scaleX)) / 2)) &&
                        (Math.abs(curTarget.Y - curBullet.Y) < (((curTarget.image.width * curTarget.scaleX) +
                            (curBullet.image.width * curBullet.scaleX)) / 2))) {
                        curTarget.isActive = false;
                        curBullet.isActive = false;

                        this.particlePool.explodeAt(curTarget.X, curTarget.Y);
                    }
                }
            }
        }
    });
});
