//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(['Assets', 'MainAnimationList', 'Helpers/Color', 'Sprites/String', 'Animation/Sprite'],
function (Assets, MainAnimationList, Color, SpriteString, Sprite) {
    "use strict";

    var isActive = false,
        ctx,
        canvas,
        curTime,
        mainAnimationList,
        gameIsInit = false;

    function mainLoop() {
        update();
        clear();
        draw();

        isActive && window.requestAnimationFrame(mainLoop);
    }

    function clear() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }

    function drawLoadingScreen(percentage) {
        clear();
        ctx.save();
        ctx.fillStyle = '#000000';
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        var mid_x = canvas.width / 2;
        var mid_y = canvas.height / 2;
        var progress_bar_width = 200;
        var progress_bar_height = 50;
        var progress_bar_padding = 5;

        ctx.fillStyle = '#FFFFFF';

        ctx.fillRect(mid_x - progress_bar_width / 2, mid_y - progress_bar_height / 2,
                     progress_bar_width, progress_bar_height);

        ctx.fillStyle = '#FF0000';
        ctx.fillRect(mid_x - progress_bar_width / 2 + progress_bar_padding,
                     mid_y - progress_bar_height / 2 + progress_bar_padding,
                     (progress_bar_width - progress_bar_padding * 2) * percentage,
                     progress_bar_height - progress_bar_padding * 2);

        ctx.restore();

    }

    function assetsLoaded() {
        console.log("Assets loaded");
        mainAnimationList = new MainAnimationList();
        exports.mainAnimationList = mainAnimationList;

        Assets.music.background.play();

        var d = new Date();
        curTime = d.getMilliseconds();
        isActive = true;
        initOtherStuff();
        mainLoop();
    }

    function initOtherStuff() {
        initBackground();
        mainAnimationList.initPools();
        initStrings();
    }

    function initBackground() {
        mainAnimationList.add(new Sprite({
            image: Assets.sprites.starField,
            X: canvas.width / 2,
            Y: canvas.height / 2,
            scaleX: 4.5,
            scaleY: 4.5,
            Da: Math.random() * 0.0001 - 0.00005
        }));

    }

    function initStrings() {
        mainAnimationList.add(new SpriteString({
            text: "HTML5 Programming at FAU",
            fillStyle: Color.randGradient(ctx),
            scaleX: 1.5,
            scaleY: 1.5,
            Y: canvas.height * 0.1,
            X: canvas.width * 0.5
        }));

        mainAnimationList.add(new SpriteString({
            text: "Dr. Thomas Fernandez",
            X: canvas.width * 0.5,
            Y: canvas.height * 0.2,
            scaleX: 1,
            scaleY: 1,
            fillStyle: Color.randGradient(ctx)
        }));


    }

    function initGame() {
        if (gameIsInit)
            return;

        gameIsInit = true;

        canvas = document.getElementById('canvas');
        ctx = canvas.getContext('2d');
        ctx.canvas.width = window.innerWidth;
        ctx.canvas.height = window.innerHeight;
        ctx.save();

        exports.canvas = canvas;
        exports.ctx = ctx;

        // Load the assets, and callback with "assetsLoaded" once everything's done
        // to start the main loop.
        drawLoadingScreen(0);
        Assets.load(assetsLoaded, drawLoadingScreen);
    }

    function update() {

        var d = new Date(),
            oldTime = curTime;

        curTime = d.getMilliseconds();
        var elapsedTime = curTime - oldTime;
        if (elapsedTime < 0) elapsedTime += 1000;

        mainAnimationList.update(elapsedTime);

        var freq = 0.002;

        if (Math.random() < freq) {
            Assets.play_sound_with_volume(Assets.audio.bell, 0.5);
        }
    }

    function draw() {
        mainAnimationList.draw(ctx);
    }

    var exports = {
        init: initGame
    };

    return exports;

});
