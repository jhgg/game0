﻿//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/Pool", "Sprites/Particle"], function (AnimationPool, Particle) {
    "use strict";

    var twoPI = Math.PI * 2;

    return AnimationPool.extend({
        init: function (count) {
            this._super();

            for (var i = 0; i < count; i++)
                this.add(new Particle());
        },

        explodeAt: function (x, y) {
            for (var k = 0; k < 100; k++) {
                var p = this.get();
                if (!p)
                    return;

                p.X = x;
                p.Y = y;
                var a = Math.random() * twoPI;
                var r = Math.random() + 0.4;
                p.Dx = Math.cos(a) * r;
                p.Dy = Math.sin(a) * r;
                var s = 2.0 + Math.random() * 2;
                p.scaleX = s;
                p.scaleY = s;
                p.isActive = true;
            }
        }
    });
});