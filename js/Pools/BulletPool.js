//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/Pool", "Sprites/Bullet"], function (AnimationPool, Bullet) {
    "use strict";

    return AnimationPool.extend({
        init: function (count) {
            this._super();

            for (var i = 0; i < count; i++) {
                this.add(new Bullet());
            }

            this.changeState("ready");// = "ready";
            this.gunCoolTime = 100;
        },

        specialUpdate: function () {
            if (this.state === "unready" && this.timeInState > this.gunCoolTime)
                this.changeState("ready");
        },

        get: function () {
            if (this.state === "ready") {
                this.changeState("unready");
                return this._super();
            }
            return null;
        }
    })
});
