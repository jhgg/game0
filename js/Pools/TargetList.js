﻿//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/Pool", "Sprites/Enemy1", "require"], function (AnimationPool, Enemy1, require) {
    "use strict";

    var twoPI = Math.PI * 2;

    return AnimationPool.extend({
        init: function (count) {
            this._super();

            count = count || 20;

            for (var i = 0; i < count; i++)
                this.add(new Enemy1())

        },

        specialUpdate: function TP_Update (elapsedTime) {
            if (Math.random() * 100 >= 101.0) return;

            var newEnemy = this.get();
            if (!newEnemy) return;

            var speed = 0.5,
                spread = 2,
                minScale = 0.5,
                maxScale = 1.0,
                s = Math.random() * maxScale + minScale,
                canvas = require("Game").canvas;

            newEnemy.edgeMode = "bounce";
            newEnemy.X = Math.random() * canvas.width * spread + canvas.width * (spread / 2);
            newEnemy.Y = Math.random() * canvas.height * spread + canvas.height * (spread / 2);
            newEnemy.Dx = Math.random() * speed + (speed / 2);
            newEnemy.Dy = Math.random() * speed + (speed / 2);
            newEnemy.scaleX = s;
            newEnemy.scaleY = s;
            newEnemy.gravety = 0.0;
            newEnemy.friction = 1.0;
            newEnemy.isActive = true;
            newEnemy.A = Math.random() * twoPI;
            newEnemy.Da = (Math.random() * 0.0002) + 0.0001;
            newEnemy.DDa = (Math.random() * 0.000002) + 0.000001;

            if (Math.random() < 0.5) newEnemy.Da *= -1;
            if (Math.random() < 0.5) newEnemy.DDa *= -1;

        }
    })
});