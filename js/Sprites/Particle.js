﻿//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/Sprite", "Assets"], function (Sprite, Assets) {
    "use strict";

    return Sprite.extend({
        init: function () {
            this._super({
                edgeMode: "inactive",
                isActive: false,
                friction: 0.97,
                gravity: 0,
                image: Assets.sprites.particle
            })
        },

        specialUpdate: function () {
            if ((this.Dx * this.Dx + this.Dy * this.Dy) < 0.1) this.isActive = false;
            this.scaleX *= 0.95;
            this.scaleY *= 0.95;
        }
    });

});

