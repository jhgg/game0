//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/Sprite", "lib/underscore"], function (Sprite, _) {
    "use strict";

    var twoPI = Math.PI * 2;

    return Sprite.extend({
        init: function (args) {
            _.defaults(this, args, {
                font: "bold 36px Tahoma",
                textAlign: "center",
                fillStyle: "black",
                useShadow: true,
                Da: 0
            });
            this._super(false);
        },

        draw: function spriteStringDraw(ctx) {
            if (!this.isActive) return;

            ctx.save();
            ctx.setTransform(1, 0, 0, 1, 0, 0);
            ctx.translate(this.X, this.Y);
            ctx.scale(this.scaleX, this.scaleY);
            ctx.rotate(this.A);
            //ctx.translate(-this.image.width / 2, -this.image.height / 2);
            ctx.globalAlpha = this.opacity;
            ctx.textAlign = this.textAlign;
            ctx.font = this.font;
            ctx.fillStyle = this.fillStyle;
            if (this.useShadow) {
                ctx.shadowOffsetX = 4;
                ctx.shadowOffsetY = 4;
                ctx.shadowBlur = 6;
                ctx.shadowColor = 'black';
            }
            ctx.fillText(this.text, 0, 0);
            if (this.useShadow) ctx.shadowColor = 'transparent';
            ctx.restore();
        },

        update: function spriteStringUpdate(elapsedTime) {
            if (!this.isActive) return;

            this.X += this.Dx * elapsedTime;
            this.Y += this.Dy * elapsedTime;

            //if (this.edgeMode == "bounce") {
            //    if (this.X < 0 + ((this.image.width * this.scaleX) / 2)) this.Dx = Math.abs(this.Dx);
            //    if (this.Y < 0 + ((this.image.height * this.scaleY) / 2)) this.Dy = Math.abs(this.Dy);
            //    if (this.X >= gW - ((this.image.width * this.scaleX) / 2)) this.Dx = Math.abs(this.Dx) * -1;
            //    if (this.Y >= gH - ((this.image.height * this.scaleY) / 2)) this.Dy = Math.abs(this.Dy) * -1;
            //}

            this.A += this.Da;
            if (this.A > twoPI) this.A -= twoPI;
            if (this.A < -twoPI) this.A += twoPI;

        }
    })
});