//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/Sprite", "Assets"], function (Sprite, Assets) {
    "use strict";

    return Sprite.extend({
        init: function () {
            this._super({
                X: 400,
                Y: 400,
                friction: 1.0,
                image: Assets.sprites.bullet,
                edgeMode: "inactive",
                isActive: false
            });

            console.log("Bullet", this.isActive);
        }
    });
});