//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************


define(["Animation/Sprite", "Assets", "Helpers/Keys", "Pools/BulletPool", 'require'],
function (Sprite, Assets, Keys, BulletPool, require) {
    "use strict";

    return Sprite.extend({
        init: function () {
            this._super({
                X: 400,
                Y: 400,
                friction: 0.96,
                image: Assets.sprites.ship
            });
        },

        specialUpdate: function shipUpdate() {
            if (Keys.isKeyPressed(Keys.keys.RIGHT_ARROW)) this.A += 0.03;
            if (Keys.isKeyPressed(Keys.keys.LEFT_ARROW)) this.A -= 0.03;

            if (Keys.isKeyPressed(Keys.keys.UP_ARROW)) {
                this.Dx += Math.cos(this.A) * 0.1;
                this.Dy += Math.sin(this.A) * 0.1;
            }

            if (Keys.isKeyPressed(Keys.keys.SPACE)) {
                var bullet = require('Game').mainAnimationList.bulletPool.get();
                if (!bullet)
                    return;

                bullet.isActive = true;
                bullet.A = this.A;
                bullet.X = this.X + Math.cos(bullet.A) * (this.image.width * this.scaleX) / 2.0;
                bullet.Y = this.Y + Math.sin(bullet.A) * (this.image.width * this.scaleX) / 2.0;

                bullet.Dx = Math.cos(bullet.A); // * 1.0
                bullet.Dy = Math.sin(bullet.A); // * 1.0
            }
        }
    });
});

