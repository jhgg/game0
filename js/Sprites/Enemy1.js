﻿//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/Sprite", "require", "Assets"], function (Sprite, require, Assets) {
    "use strict";

    var twoPI = Math.PI * 2;

    return Sprite.extend({
        init: function () {
            var speed = 0.5,
                minScale = 0.5,
                maxScale = 1.0,
                s = Math.random() * maxScale + minScale,
                game = require('Game');

            this._super({
                X: Math.random() * game.canvas.width,
                Y: Math.random() * game.canvas.height,
                Dx: Math.random() * speed - (speed / 2),
                Dy: Math.random() * speed - (speed / 2),
                scaleX: s,
                scaleY: s,
                gravity: 0.00002,
                isActive: false,
                DDa: 0.001,
                image: Assets.sprites.enemyShip
            });
        },

        specialUpdate: function (elapsedTime) {
            this.Da += this.DDa * elapsedTime;
            if (Math.abs(this.Da) > twoPI) this.DDa *= -1;

        }
    })
});
