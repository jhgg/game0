define(["Helpers/ImageLoader", "Helpers/BufferLoader", 'lib/underscore'], function (ImageLoader, BufferLoader, _) {
    "use strict";

    var i = 0;

    var AudioContext = window.AudioContext || window.webkitAudioContext;
    var context = new AudioContext();

    return {
        sprites: {}, // populated by load function
        audio: {},
        music: {},

        // Define sprites to be loaded here...
        sprite_urls: {
            enemyShip: "images/enemyShip2.png",
            lotus: "images/Lotus2.png",
            starField: "images/NasaStarFeild004.png",
            galaxy: "images/galaxyCW.png",
            galaxySmall: "images/galaxyM51Small.png",
            ship: "images/ship5.png",
            bullet: "images/VacumTube003.png",
            particle: "images/particle.png"
        },

        // Define audio to be loaded here.
        audio_urls: {
            bell: "audio/boxing_bell.wav"
        },

        music_urls: {
            background: "audio/Under Oriental Skies (Theme).mp3"
        },

        // Convenience function for playing a sound.
        play_sound: function (buffer) {
            var source = context.createBufferSource();
            source.buffer = buffer;
            source.connect(context.destination);
            source.start(0);
        },

        // Convenience function for playing a sound with a specific volume.
        play_sound_with_volume: function (buffer, volume) {
            var source = context.createBufferSource(),
                gainNode = context.createGain();

            source.buffer = buffer;
            source.connect(gainNode);
            gainNode.connect(context.destination);
            gainNode.gain.value = volume;

            source.start(0);

            // Returns a function that allows you to set the volume of the sound...
            return function (vol) {
                gainNode.gain.value = vol;
            }
        },

        // Load all assets.
        load: function (all_assets_loaded, progress) {
            var that = this,
                total_assets_to_load = _.size(this.audio_urls) + _.size(this.sprite_urls),
                total_loaded = 0;

            function progress_inner() {
                total_loaded++;
                progress && progress(total_loaded / total_assets_to_load);
            }


            ImageLoader.loadImagesMap(this.sprite_urls, function (sprites) {
                that.sprites = sprites;
                if (++i == 2)
                    all_assets_loaded();
            }, progress_inner);

            BufferLoader.loadBufferMap(context, this.audio_urls, function (audio) {
                that.audio = audio;
                if (++i == 2)
                    all_assets_loaded();
            }, progress_inner);

            _.each(this.music_urls, function(v, k) {
                that.music[k] = new Audio(v);
            })

        }
    };
});