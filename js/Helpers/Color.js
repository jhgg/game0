//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define({
    randGradient: function (g) {
        var gW = g.canvas.width, gH = g.canvas.height;
        var gradient = g.createLinearGradient(Math.random() * gW, Math.random() * gH, Math.random() * gW, Math.random() * gH);
        gradient.addColorStop(0.0, this.randColor());
        gradient.addColorStop(0.5, this.randColor());
        gradient.addColorStop(1.0, this.randColor());
        return gradient;
    },

    tColor: function (r, g, b, a) {
        var result = 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
        return result;
    },

    randLightColor: function () {
        var rC = Math.floor(Math.random() * 256),
            gC = Math.floor(Math.random() * 256),
            bC = Math.floor(Math.random() * 256);

        while (Math.max(rC, gC, bC) < 200) {
            rC = Math.floor(Math.random() * 256);
            gC = Math.floor(Math.random() * 256);
            bC = Math.floor(Math.random() * 256);
        }

        return this.tColor(rC, gC, bC, 1.0);
    },

    randColor: function () {
        var rC = Math.floor(Math.random() * 256),
            gC = Math.floor(Math.random() * 256),
            bC = Math.floor(Math.random() * 256);
        return this.tColor(rC, gC, bC, 1.0);
    }
});
