//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(['Animation/Object', 'lib/underscore', 'require'], function (AnimationObject, _, require) {
    "use strict";

    var twoPI = Math.PI * 2;

    return AnimationObject.extend({
        init: function (args) {
            _.defaults(this, args, {
                X: 0,
                Y: 0,
                Dx: 0,
                Dy: 0,
                A: 0,
                Da: 0,
                scaleX: 1,
                scaleY: 1,
                opacity: 1.0,
                edgeMode: "bounce",
                useShadow: false,
                gravity: 0.0,
                friction: 1.0,
                isActive: true
            });
        },

        draw: function spriteDraw(ctx) {
            if (!this.isActive) return;

            ctx.save();
            ctx.setTransform(1, 0, 0, 1, 0, 0);
            ctx.translate(this.X, this.Y);
            ctx.scale(this.scaleX, this.scaleY);
            ctx.rotate(this.A);
            ctx.translate(-this.image.width / 2, -this.image.height / 2);
            ctx.globalAlpha = this.opacity;
            if (this.useShadow) {
                ctx.shadowOffsetX = 4;
                ctx.shadowOffsetY = 4;
                ctx.shadowBlur = 6;
                ctx.shadowColor = 'black';
            }

            ctx.drawImage(this.image, 0, 0);
            if (this.useShadow) ctx.shadowColor = 'transparent';
            ctx.restore();

        },

        update: function spriteUpdate(elapsedTime) {
            var canvas = require("Game").canvas;

            this.timeInState += elapsedTime;
            if (!this.isActive) return;

            this.Dx *= this.friction;
            this.Dy *= this.friction;
            if (this.gravity != 0)
                this.Dy += this.gravity * elapsedTime;

            this.X += this.Dx * elapsedTime;
            this.Y += this.Dy * elapsedTime;

            if (this.edgeMode === "bounce") {
                if (this.X < 0 + ((this.image.width * this.scaleX) / 2)) this.Dx = Math.abs(this.Dx);
                if (this.Y < 0 + ((this.image.height * this.scaleY) / 2)) this.Dy = Math.abs(this.Dy);
                if (this.X >= canvas.width - ((this.image.width * this.scaleX) / 2)) this.Dx = Math.abs(this.Dx) * -1;
                if (this.Y >= canvas.height - ((this.image.height * this.scaleY) / 2)) this.Dy = Math.abs(this.Dy) * -1;
            }
            else if (this.edgeMode === "inactivate") {
                var wScaled = this.image.width * this.scaleX,
                    hScaled = this.image.height * this.scaleY;

                if (this.X < -wScaled ||
                    this.Y < -hScaled ||
                    this.X >= canvas.width + wScaled ||
                    this.Y >= canvas.height + hScaled
                    ) this.isActive = false;

            }

            this.A += this.Da * elapsedTime;
            if (this.A > twoPI) this.A -= twoPI;
            if (this.A < -twoPI) this.A += twoPI;

            if (this.specialUpdate) this.specialUpdate(elapsedTime);
        }
    })
});