//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************


define(['lib/class'], function (Class) {
    "use strict";

    return Class.extend({
        init: function () {
            this.isActive = true;
            this.state = "inactive";
            this.timeInState = 0.0;
        },

        changeState: function (newState) {
            this.state = newState;
            this.timeInState = 0.0;
        }
    })
});