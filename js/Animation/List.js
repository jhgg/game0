//***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/Object"], function (AnimationObject) {
    "use strict";

    return AnimationObject.extend({
        init: function () {
            this._super();
            this.data = [];
        },

        add: function AL_Add(obj) {
            if (obj.update && obj.draw)
                this.data.push(obj);
            else console.log("Could not add", obj);
        },

        draw: function AL_Draw(ctx) {
            if (!this.isActive) return;
            for (var i = 0, len = this.data.length; i < len; i++)
                this.data[i].draw(ctx);
        },

        update: function AL_Update(elapsedTime) {
            this.timeInState += elapsedTime;
            if (this.isActive)
                for (var i = 0, len = this.data.length; i < len; i++)
                    this.data[i].update(elapsedTime)

            if (this.specialUpdate) this.specialUpdate(elapsedTime);
        }
    });
});