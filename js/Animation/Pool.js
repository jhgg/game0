 //***********************************************************
// Copyright 2013 Dr. Thomas Fernandez
//       All rights reserved.
//***********************************************************

define(["Animation/List"], function (AnimationList) {
    "use strict";

    return AnimationList.extend({
        init: function () {
            this._super();
            this.lastGottenIndex = 0;
        },

        get: function () {
            var ix = this.lastGottenIndex,
                data_length = this.data.length;

            for (var i = 0; i < data_length; i++) {
                // Let's try not to use modulus.
                ix++;
                ix %= data_length;

                var obj = this.data[ix];
                if (!obj.isActive) {
                    this.lastGottenIndex = ix;
                    return obj;
                }
            }

            return null;
        }

    });
});